#! /usr/bin/env python3

from random import seed, shuffle
import sys
import os
from csv import writer, QUOTE_MINIMAL
from datetime import date, datetime
import pprint


################################################################################
#
#
#   Class Definitions
#	Player
#		data about an individual player
#	Roster
#		data about the entire team roster
#	Position_Assignment
#		data about what player is playing which position
#
################################################################################

class Player(dict):
	headers = ['name','catcher','number']
	def __init__(self, data):
		pairs = zip(self.headers, data.split(','))
		self.player = dict(pairs)
		for key,value in self.player.items():
			super().__setitem__(key,value.strip())

class Roster(object):
	def __init__(self,path):
		self.path = path
		self.members = self.build_member_list(self.path)
		self.catchers = self.build_catcher_list(self.members)
	
	def build_member_list(self, path):
		with open(path, 'r') as file:
			return [Player(line) for line in file]
	def build_catcher_list(self, members):
		return [player for player in members if player['catcher'] == 'Y']

class Position_Assignment(list):
	positions = [
		'Pitcher',
		'Catcher',
		'First Base',
		'Second Base',
		'Third Base',
		'Shortstop',
		'Left field',
		'Left Center Field',
		'Right Center Field',
		'Right Field',
		'Dugout',
		'Dugout',
		'Dugout',
		'Dugout',
		'Dugout'
	]

	def __init__(self, roster, catchers, dugout, previous_infield):
		self.catcher = self.pick_catcher(catchers)
		self.dugout = self.bench([ player for player in roster.members if player not in dugout and player != self.catcher ])
		self.player_positions = self.fill_positions(self.catcher, roster.members, self.dugout,previous_infield)
		for position in zip(self.positions,self.player_positions):
			super().append(position)
	
	def bench(self, roster):
		players = roster[:]
		shuffle(players)
		return players[0:3]
			
	def pick_catcher(self, catchers):
		new_catchers = catchers[:]
		shuffle(new_catchers)
		return new_catchers[0]

	def fill_positions(self, catcher, members, dugout, previous_infield):
		is_playable = lambda player: player != catcher and player not in dugout
		is_infield = lambda player: is_playable(player) and player not in previous_infield
		available_infield_players = filter(is_infield, members)
		infield_position_candidates = list(available_infield_players)
		shuffle(infield_position_candidates)
		infield_positions = infield_position_candidates[0:5]
		outfield_positions = [player for player in members if is_playable(player) and player not in infield_positions]
		shuffle(outfield_positions)
		positions = infield_positions + outfield_positions
		positions.insert(1,catcher)
		positions.extend(dugout)
		return positions
		
################################################################################
#
#
#	Generation Logic
#
#
#
#
#
#
#
################################################################################


#######################
#	get inputs
#######################
date_seed, roster_file_path = sys.argv[1:]

roster = Roster(roster_file_path)
gameday = datetime.combine(
	datetime.fromisoformat(date_seed),
	datetime.min.time()
)
seed(gameday.timestamp())

#######################
#	initialize variables
#######################
position_assignments = []
catchers = [] 
dugout = []
previous_infield = []

printer = pprint.PrettyPrinter()

#######################
#	build innings
#######################
for inning in range(1,9):
	pa = Position_Assignment(roster,catchers if len(catchers) == 2 else roster.catchers, dugout, previous_infield)
	if pa.catcher not in catchers:
		catchers.append(pa.catcher) 
	dugout.extend(pa.dugout)
	dugout = dugout[len(roster.members)- 6:] if len(dugout) >= len(roster.members) else dugout
	position_assignments.append(pa)
	previous_infield = list(map(lambda assignment: assignment[1], [pa[0]] + pa[2:6])) # pitcher and first through short

#######################
#	output 
#######################
# create directory for the game
try:
	os.mkdir(f'./{date_seed}')
except Exception:
	print(f'./{date_seed} already exists')

# write lineup based off of position_assignments[0] except catchers go in the middle to account for dress times
with open(f'./{date_seed}/lineup','w') as lineup:
	output = writer(lineup, delimiter = '.', quoting = QUOTE_MINIMAL)	
	#lineup = map(lambda pa: pa[1]['name'],position_assignments[0])
	lineup = list(filter(lambda player: player not in catchers, map(lambda pa: pa[1],position_assignments[0])))
	lineup.insert(5, catchers[0])
	lineup.insert(6, catchers[1])
	for number, batter in enumerate(map(lambda player: player['name'], lineup), start = 1):
		output.writerow([number,batter])
# output each position assignment to a file
for inning, positions in enumerate(position_assignments, start = 1):
	with open(f'./{date_seed}/inning_{inning}', 'w') as assignments:
		output = writer(assignments, delimiter = ':', quoting = QUOTE_MINIMAL)
		for position, player in positions:
			output.writerow([position,f'\t{player["name"]}'])

